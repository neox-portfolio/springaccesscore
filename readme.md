# SpringAccessCore

SpringAccess core es un pequeño módulo para implementación de ACL

## Instalación

Crear una base de datos en SQL Server Express 2022

```bash
* git clone https://gitlab.com/neox-portfolio/springaccesscore.git

* instalar las dependencias del maven POM.xml

* CREATE DATABASE SpringAccessCore;

* Abrir proyecto con Visual Studio Code o IntelliJ Idea.

* Ejecutar Aplicación

* No se necesita importar .SQL ya que el proyecto es CodeFirst
```

## Documentación
Dentro de la carpeta postman, podrá encontrar una collection con ejemplos
funcionales de cada endpoint del API Rest.
./SpringAccessCore/postman/SpringAccessCore.postman_collection.json

## Configuración de variables

```java
aplication.properties file
spring.datasource.url= jdbc:sqlserver://mssql-177524-0.cloudclusters.net\\SQLEXPRESS:10043;encrypt=true;trustServerCertificate=true;databaseName=SpringAccessCore
spring.datasource.username= issue
spring.datasource.password= issueMorphic23@

```
no es necesario cambiarlo a un entorno local, puede funcionar con entorno remoto con las variables de bd que se encuentran.


## Características
* Inicio de sesión
* Registro
* Productos
* Categoría de productos
* Usuarios

