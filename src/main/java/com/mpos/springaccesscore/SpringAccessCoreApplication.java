package com.mpos.springaccesscore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAccessCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAccessCoreApplication.class, args);
	}

}
