package com.mpos.springaccesscore.persistence.repositories;

import com.mpos.springaccesscore.persistence.entities.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

    @Query(value = "SELECT * FROM categories WHERE email = :name", nativeQuery = true)
    Optional<CategoryEntity> findByName(String name);

}
