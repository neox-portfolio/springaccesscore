package com.mpos.springaccesscore.persistence.repositories;

import com.mpos.springaccesscore.persistence.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Query(value = "SELECT * FROM products WHERE name = :name", nativeQuery = true)
    Optional<ProductEntity> findByName(String name);

}
