package com.mpos.springaccesscore.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mpos.springaccesscore.persistence.entities.UserEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query(value = "SELECT * FROM users WHERE email = :email", nativeQuery = true)
    Optional<UserEntity> findByEmail(String email);

}
