package com.mpos.springaccesscore.services;

import com.mpos.springaccesscore.persistence.entities.UserEntity;
import com.mpos.springaccesscore.services.models.dtos.LoginDTO;
import com.mpos.springaccesscore.services.models.dtos.ResponseDTO;

import java.util.HashMap;

public interface IAuthService {

    HashMap<String, String> login(LoginDTO loginRequest) throws Exception;
    ResponseDTO register(UserEntity user) throws Exception;

}
