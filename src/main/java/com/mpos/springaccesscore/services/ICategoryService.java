package com.mpos.springaccesscore.services;

import com.mpos.springaccesscore.persistence.entities.CategoryEntity;

import java.util.List;

public interface ICategoryService {
    CategoryEntity addCategory(CategoryEntity category);

    List<CategoryEntity> findAllCategories();

    CategoryEntity findCategoryById(Long id);

    CategoryEntity updateCategory(Long id, CategoryEntity categoryDetails);

    void deleteCategory(Long id);
}
