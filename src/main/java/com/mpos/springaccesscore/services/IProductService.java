package com.mpos.springaccesscore.services;

import com.mpos.springaccesscore.persistence.entities.ProductEntity;

import java.util.List;

public interface IProductService {
    ProductEntity addProduct(ProductEntity product);

    List<ProductEntity> findAllProducts();

    ProductEntity findProductById(Long id);

    ProductEntity updateProduct(Long id, ProductEntity productDetails);

    void deleteProduct(Long id);
}
