package com.mpos.springaccesscore.services;

import com.mpos.springaccesscore.persistence.entities.UserEntity;

import java.util.List;

public interface IUserService {
    UserEntity addUser(UserEntity user);

    List<UserEntity> findAllUsers();

    UserEntity findUserById(Long id);

    UserEntity updateUser(Long id, UserEntity userDetails);

    void deleteUser(Long id);
}
