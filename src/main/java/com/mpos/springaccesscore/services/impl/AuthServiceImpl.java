package com.mpos.springaccesscore.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpos.springaccesscore.persistence.entities.RoleEntity;
import com.mpos.springaccesscore.persistence.entities.UserEntity;
import com.mpos.springaccesscore.persistence.repositories.RoleRepository;
import com.mpos.springaccesscore.persistence.repositories.UserRepository;
import com.mpos.springaccesscore.services.IAuthService;
import com.mpos.springaccesscore.services.IJWTUtilityService;
import com.mpos.springaccesscore.services.models.dtos.LoginDTO;
import com.mpos.springaccesscore.services.models.dtos.ResponseDTO;
import com.mpos.springaccesscore.services.models.validations.UserValidations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements IAuthService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private IJWTUtilityService jwtUtilityService;

    @Autowired
    private UserValidations userValidations;

    @Override
    public HashMap<String, String> login(LoginDTO loginRequest) throws Exception {
        try {
            HashMap<String, String> jwt = new HashMap<>();
            Optional<UserEntity> user = userRepository.findByEmail(loginRequest.getEmail());

            if (user.isEmpty()) {
                jwt.put("error", "User not registered!");
                return jwt;
            }
            if (verifyPassword(loginRequest.getPassword(), user.get().getPassword())) {
                jwt.put("jwt", jwtUtilityService.generateJWT(user.get().getId(), user.get().getRoles()));



            } else {
                jwt.put("error", "Authentication failed");
            }
            return jwt;
        } catch (IllegalArgumentException e) {
            System.err.println("Error generating JWT: " + e.getMessage());
            throw new Exception("Error generating JWT", e);
        } catch (Exception e) {
            System.err.println("Unknown error: " + e.toString());
            throw new Exception("Unknown error", e);
        }
    }

    @Override
    public ResponseDTO register(UserEntity user) throws Exception {
        try {
            ResponseDTO response = userValidations.validate(user);
            List<UserEntity> getAllUsers = userRepository.findAll();

            if (response.getNumOfErrors() > 0) {
                return response;
            }

            for (UserEntity _user : getAllUsers) {
                if (_user != null && _user.getEmail().equals(user.getEmail())) {
                    response.setMessage("User already exists!");
                    return response;
                }
            }

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
            user.setPassword(encoder.encode(user.getPassword()));

            //user.setUsername(user.getRoles().toString());
           // String[] roleNames = user.getRoles().toString().replaceAll("[\\[\\]\"]", "").split(",");

           /* for (String roleName : roleNames) {
                String _roleName = roleName.trim().toUpperCase();
                RoleEntity role = roleRepository.findByName(_roleName)
                        .orElseThrow(() -> new RuntimeException("Error: Role " + _roleName + " is not found."));
                user.setRoles(Collections.singleton(role));
            }*/

            RoleEntity userRole = roleRepository.findByName(user.getUsername())
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            user.setRoles(Collections.singleton(userRole));

            userRepository.save(user);
            response.setMessage("User created successfully!");
            return response;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private boolean verifyPassword(String enteredPassword, String storedPassword) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.matches(enteredPassword, storedPassword);
    }
}
