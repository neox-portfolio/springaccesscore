package com.mpos.springaccesscore.services.impl;

import com.mpos.springaccesscore.persistence.entities.CategoryEntity;
import com.mpos.springaccesscore.persistence.repositories.CategoryRepository;
import com.mpos.springaccesscore.services.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements ICategoryService {


    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public CategoryEntity addCategory(CategoryEntity category) {

        category.setCreated_at(LocalDateTime.now());
        category.setUpdated_at(LocalDateTime.now());
        return categoryRepository.save(category);
    }

    @Override
    public List<CategoryEntity> findAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public CategoryEntity findCategoryById(Long id)  {
        try {
            Optional<CategoryEntity> category = categoryRepository.findById(id);
            return category.orElseThrow(() -> new RuntimeException("Category not found"));

        } catch (Exception e) {
            try {
                throw new Exception(e.getMessage());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public CategoryEntity updateCategory(Long id, CategoryEntity categoryDetails)  {
        CategoryEntity category = this.findCategoryById(id);
        category.setName(categoryDetails.getName());
        category.setState(categoryDetails.getState());
        category.setUpdated_at(LocalDateTime.now());
        return categoryRepository.save(category);
    }

    //implementación de softdelete
    @Override
    public void deleteCategory(Long id) {
        CategoryEntity category = this.findCategoryById(id);
        category.setDeleted(true);
        category.setDeleted_at(LocalDateTime.now());
        categoryRepository.save(category);
    }

}
