package com.mpos.springaccesscore.services.impl;

import com.mpos.springaccesscore.persistence.entities.ProductEntity;
import com.mpos.springaccesscore.persistence.repositories.ProductRepository;
import com.mpos.springaccesscore.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public ProductEntity addProduct(ProductEntity product) {

        product.setCreated_at(LocalDateTime.now());
        product.setUpdated_at(LocalDateTime.now());

        return productRepository.save(product);
    }

    @Override
    public List<ProductEntity> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public ProductEntity findProductById(Long id) {
        try {
            Optional<ProductEntity> category = productRepository.findById(id);
            return category.orElseThrow(() -> new RuntimeException("Product not found"));

        } catch (Exception e) {
            try {
                throw new Exception(e.getMessage());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

    }

    @Override
    public ProductEntity updateProduct(Long id, ProductEntity productDetails) {
        ProductEntity product = null;
        try {
            product = this.findProductById(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        product.setName(productDetails.getName());
        product.setState(productDetails.getState());
        product.setUpdated_at(LocalDateTime.now());
        return productRepository.save(product);
    }

    // soft delete
    @Override
    public void deleteProduct(Long id) {
        ProductEntity product = findProductById(id);
        product.setDeleted(true);
        product.setDeleted_at(LocalDateTime.now());
        productRepository.save(product);
    }

}
