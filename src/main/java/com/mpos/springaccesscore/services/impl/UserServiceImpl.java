package com.mpos.springaccesscore.services.impl;

import com.mpos.springaccesscore.persistence.entities.ProductEntity;
import com.mpos.springaccesscore.persistence.entities.UserEntity;
import com.mpos.springaccesscore.persistence.repositories.RoleRepository;
import com.mpos.springaccesscore.persistence.repositories.UserRepository;
import com.mpos.springaccesscore.services.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserEntity addUser(UserEntity user) {
        return null;
    }

    @Override
    public List<UserEntity> findAllUsers(){
        return userRepository.findAll();
    }

    @Override
    public UserEntity findUserById(Long id) {
        return null;
    }

    @Override
    public UserEntity updateUser(Long id, UserEntity userDetails) {
        return null;
    }

    // soft delete
    @Override
    public void deleteUser(Long id) {
        UserEntity user = findUserById(id);
        user.setDeleted(true);
        user.setDeleted_at(LocalDateTime.now());
        userRepository.save(user);
    }
}
