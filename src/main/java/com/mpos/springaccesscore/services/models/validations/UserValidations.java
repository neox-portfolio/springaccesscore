package com.mpos.springaccesscore.services.models.validations;

import com.mpos.springaccesscore.persistence.entities.UserEntity;
import com.mpos.springaccesscore.services.models.dtos.ResponseDTO;

public class UserValidations {

    public ResponseDTO validate(UserEntity user){

        ResponseDTO response = new ResponseDTO();

        response.setNumOfErrors(0);

        if (user.getFullname() == null || user.getFullname().length() < 3 || user.getFullname().length() > 15){
            response.setNumOfErrors(response.getNumOfErrors() + 1);
            response.setMessage("El campo nombre completo no puede ser nulo, tampoco puede tener menos de 3 caracteres ni mas de 15");
        }

        if (user.getUsername() == null || user.getUsername().length() < 3 || user.getUsername().length() > 30){
            response.setNumOfErrors(response.getNumOfErrors() + 1);
            response.setMessage("El campo nombre de usuario no puede ser nulo, tampoco puede tener menos de 3 caracteres ni mas de 30");
        }

        if (
                user.getEmail() == null ||
                        !user.getEmail().matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")
        ){
            response.setNumOfErrors(response.getNumOfErrors() + 1);
            response.setMessage("El campo email no es correcto");
        }

        if(
                user.getPassword() == null ||
                        !user.getPassword().matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,16}$")
        ){
            response.setNumOfErrors(response.getNumOfErrors() + 1);
            response.setMessage("La contraseña debe tener entre 8 y 16 caracteres, al menos un número, una minúscula y una mayúscula.");
        }

        return response;

    }

}
